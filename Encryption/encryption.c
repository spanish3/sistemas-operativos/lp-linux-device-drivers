#include <linux/init.h>
#include <linux/kernel.h>		// Estamos haciendo trabajo de kernel
#include <linux/module.h>		// Espesíficamente un módulo
#include <linux/fs.h>           // Para poder utilizar struct file, struct inode, struct file_operations
#include <linux/uaccess.h>		// Para poder usar get_user y put_user

//Definiciones

#define BUF_LEN 80        // Tamaño de mensaje aceptado
#define DEVICE_NAME "encryption_char"
#define DEVICE_FILE_NAME "encryption_char"
#define MAJOR_NUM 143

static int valor = 5;      // Valor para cifrado 
//static int MayorNum=0;      // Mayor number del módulo // Definimos el Mayor Number, el registro fallará si ya es utilizado
//static int minorNum=0;      // Minor number del módulo // Por defectoe l primero es cero
static int Device_Open = 0;

static char decrypted_msg [BUF_LEN];
static char *decrypted_msg_ptr;

static int encrypt_open(struct inode *inode, struct file *file)
{
	//static int counter = 0;

	if (Device_Open)
		return -EBUSY;

	Device_Open++;
	//sprintf("I already told you %d times Hello world!\n", counter++);
    decrypted_msg_ptr = decrypted_msg;
	try_module_get(THIS_MODULE);

	return 0;
}
static int encrypt_close(struct inode *inode, struct file *file)
{
	/*int k = 0;
	for (k = 0; k < BUF_LEN; k++)
	{
		printk(KERN_ALERT "msg[%d]: %c\n",k,decrypted_msg[k]);
	}*/
	/* Este bucle for que se encuentra comentado es para mostrar que si por ejemplo primero se escriben 10 caracteres
	 * en el buffer (ver funciones read/write) y luego se se sobrescribe con 5 caracteres, 3 de los 10 caracteres iniciales
	 * permanecen el el buffer, es decir el buffer no se limpia al sobrescribirse. Para evitar esto se deberían hacer null
	 * todos los lugares desde len + 2 hasta BUF_LEN, pero como esta característica no presenta inconvenientes a los efectos
	 * del trabajo práctico se dejará como está.
	 */
	Device_Open--;		/* Ahora que se cerró el archivo, alguien más puede abrirlo */
                        /* Se decremente Device_Open para que habilitar open*/
	module_put(THIS_MODULE);
	return 0;
}

static ssize_t encrypt_read(struct file *filp,char *buffer,size_t length,loff_t * offset)
{
    // struct file *flip, ver include/linux/fs.h
    // *buffer es el buffer a llenar de datos
    // lenght es la longitud del buffer
	// loff_t * offset es el desplazamiento para ver dónde se está leyendo/escribiendo en el archivo 
	
	int bytes_read = 0;     // Se define bytes_read como la cantidad de bytes que se escribirán en el buffer.

	if (*decrypted_msg_ptr == 0)
    {
		return 0;       // Si nos encontramos al final del mensaje, devuelve 0 señalando el final del archivo.
    }                   // No se escribió data en el buffer de espacio de usuario

	//Realmente poner datos en el buffer:

	while (length && *decrypted_msg_ptr) {

		/* 
		   El buffer está en el segmento de datos a nivel de usuario, no a nivel de kernel, por ello no se puede usar 
		   una asignación con punteros si no que se usa put_user, que copia datos del segmento de datos a nivel de 
		   kernel al segmento de datos a nivel de usuario 
		*/
		put_user(*(decrypted_msg_ptr++), buffer++);

		length--;
		bytes_read++;
	}
	return bytes_read;      // Se devuelve la cantidad de bytes escritos en el buffer, 
                            // esto lo hacen la mayoría de las funciones read
}

static ssize_t encrypt_write(struct file *f, const char __user *buf, size_t len, loff_t *off)
{
	char auxbuf[BUF_LEN];
    char *auxbuf_ptr;
	int i = 0, j = 0;
	if(len > BUF_LEN)
	{
		printk(KERN_ALERT "Demasiados caracteres, solo 80 disponibles");
		//return -1
	}
	for (i = 0; i < len && i < BUF_LEN; i++)
	{
		get_user(auxbuf[i], buf + i);
	}
	auxbuf[len] = '\n';			// Añadimos un fin de linea con fines de claridad de lectura, quitarlo si llegase a ser indeseable para el programa de usuario
	auxbuf[len + 1] = '\0';		// Añadimos \0 indicando el fin de la cadena de caracteres.
	auxbuf_ptr = auxbuf;
    for(j = 0; j< len && j< BUF_LEN; j++)
    {
        decrypted_msg[j] = auxbuf[j] + valor;
    }
	decrypted_msg[len] = '\n';			// Añadimos un fin de linea con fines de claridad de lectura, quitarlo si llegase a ser indeseable para el programa de usuario
	decrypted_msg[len + 1] = '\0';		// Añadimos \0 indicando el fin de la cadena de caracteres.
    decrypted_msg_ptr = decrypted_msg;
	return i;
}

// Asignación /acciones a realizar con el CDF/-/Funciones de este CDD/
// CDF = Character Device File
// CDD = Character Device Driver

static struct file_operations encrypt_fops =
{
    .owner = THIS_MODULE,
    .open = encrypt_open,
    .release = encrypt_close,
    .read = encrypt_read,
    .write = encrypt_write
};

//Funciones que se ejecutarán al cargar (init) o descargar (exit) el módulo

static int encrypt_init(void)
{
    int ret_val;
	ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &encrypt_fops);

	if (ret_val < 0) 
    {
		printk(KERN_ALERT "%s ha fallado con %d\n","Disculpe, el registro del dispositivo de caracteres ", ret_val);
		return ret_val;
	}

	printk(KERN_INFO "%s El major device number es %d.\n","Registro exitoso", MAJOR_NUM);
	printk(KERN_INFO "Para hablar con el driver,\n");
	printk(KERN_INFO "se debe crear un archivo de driver. \n");
	printk(KERN_INFO "Se sugiere usar:\n");
	printk(KERN_INFO "mknod /dev/%s c %d 0\n", DEVICE_FILE_NAME, MAJOR_NUM);

	return 0;
}
static void encrypt_exit(void)
{
    unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
    printk(KERN_INFO "chardev: Adios mundo cruel..!!\n");
}
module_init(encrypt_init);
module_exit(encrypt_exit);

//Licencia y Documentación

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Grupo: Briasco, Pardina");
MODULE_DESCRIPTION("Driver de caracter para encriptar");