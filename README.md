# lp-linux-device-drivers

## Clonar Repositorio

```
cd existing_repo
git remote add origin https://gitlab.com/spanish3/sistemas-operativos/lp-linux-device-drivers.git
git branch -M main
git pull origin main
```

## Sistemas Operativos I - Trabajo Práctico N° 3: Desarrollo de módulos para kernel de Linux

*Año 2019*

### Introducción
En este trabajo práctico se desarrollarán dos simples módulos para insertar en el kernel de Linux. Estos módulos emularán el funcionamiento de dos dispositivos de caracteres. Uno de los dispositivos realizará una encriptación simple de los caracteres que se escriben en él. El otro módulo realizará la desencriptación de los caracteres que se escriben en él.

### Marco Teórico
Como se sabe el kernel es el núcleo del sistema operativo, encargado de gestionar los recursos del sistema y realizar toda la comunicación segura entre el software y el hardware. Ahora bien, los sistemas operativos basados en el kernel de Linux funcionan correctamente con una gran variedad de plataformas de hardware (diferentes: placas de video, sonido, red, discos duros, etc). Además, la mayor parte de las aplicaciones de estos sistemas operativos son desarrolladas sin conocer las características del hardware sobre el que serán utilizadas. Como encargado de la comunicación entre software y hardware el kernel debe contener las instrucciones para que las aplicaciones se comuniquen con estos dispositivos. Pero si el kernel contemplase todos los dispositivos existentes, sería extremadamente extenso (lo cual no es deseable) o bien cada vez que se conecta un dispositivo que no está contemplado en el kernel en ese instante se debería agregar el código para manejar este dispositivo al kernel y reiniciar el sistema (pues el kernel ha sido modificado). Para evitar esto existen los módulos de kernel o controladores de dispositivos.

#### ¿Qué son los módulos de Kernel?
Son fracciones de código que pueden ser cargados y descargados del kernel si así es solicitado. Extienden la funcionalidad del kernel sin necesidad de reiniciar el sistema. Una clase de módulo de kernel es el controlador de dispositivos (device driver).
Los controladores de dispositivos se dividen en dos tipos:
- Block Device Drivers: Contienen un buffer para peticiones y pueden elegir el mejor orden para responderlas. Manejan dispositivos de almacenamiento y de red. Sólo pueden aceptar entradas y generar salidas en bloques (cuyo tamaño varía según el dispositivo).
- Character Device Drivers (CDD): Todos los dispositivos que no son de red ni de almacenamiento son controlados por un CDD. Pueden usar tantos bytes como se desee (no están restringidos por un bloque definido). Es el tipo utilizado en este trabajo práctico.

![Kernel_Modules](Img/Picture1.png)

Para que las aplicaciones de usuario puedan comunicarse un el controlador de dispositivos de caracteres (CDD), se requiere de un archivo de dispositivos (CDF: Character Device File):
De esta forma, las aplicaciones de usuario se comunican con el CDF y en base a las operaciones de lectura, escritura, etc que sean efectuadas sobre el archivo, el CDD realizará su función.

Los archivos de dispositivos tienen dos números asociados:
- Major number: Indica qué driver es utilizado para acceder a cierto hardware (cada Device Driver tiene un único mayor number). Si dos o más archivos correspondientes a dispositivos indican el mismo major number, esto significa que son controlados por el mismo driver.
- Minor number: Sirve para distinguir entre distintos dispositivos que son controlados por el mismo driver.
Ejemplo:

![Example_1](Img/Picture2.png)

    Se observa el caso de los archivos de dispositivo para tres particiones de un disco duro. En la quinta y sexta columna hay dos números separados por una coma. El primero (3) corresponde al major number del controlador utilizado para manejar estas particiones y como las tres utilizan el mismo controlador, este número se repite. El segundo es distinto para cada partición pues se debe diferenciar los archivos para comunicarse específicamente con uno o con otro (son archivos distintos).

### Desarrollo
Un módulo de kernel debe contener al menos dos funciones: 
- Una "start" (de inicialización) llamada init_module(). Esta es llamada cuando un módulo de kernel es cargado. Generalmente registra algún controlador o reemplaza alguna función del kernel con su propio código.
- Una "end" (cleanup) llamada cleanup_moudle() que es llamada justo antes de que un módulo de kenrel sea removido (rmmod). (Deshace lo que init_module() haya hecho). Todos los módulos deben incluir linux/module.h
En coherencia con esto, incluimos dicha librería y escribimos el módulo inicial con estas funciones:

![Code_1](Img/Picture3.png)

La función register_chrdev registra un dispositivo de caracteres con un determinado nombre major number. 
Si el registro de dicho dispositivo falla (por ejemplo si el major number ya está ocupado por otros dispositivo), esta devuelve un valor negativo, en cuyo caso se imprime un mensaje y se sale de la función init. También, register_chrdev tiene como parámetro un valor de tipo struct file_operations del que se hablará más adelante.
La función unregister_chrdev deshace lo realizado por la función anterior.
    En pocas palabras, nuestro controlador debe indicar que acciones tomar cuando una aplicación de usuario interactúa con el archivo de dispositivo de carácter, con lo cual fue necesario indicar qué funciones se ejecutarán cuando se abra, cierre, lea y escriba dicho archivo. Esto se logra mediante el struct file_operations encryp_fops: 

![Code_2](Img/Picture4.png)

Por último se pasó a escribir dichas funciones que se ejecutarán cuando se interactúe con el CDF (encrypt_open, encrypt_close, encrypt_read y encrypt_write):

![Code_3](Img/Picture5.png)

- encrypt_open y encryp_close:

No es deseable que se pueda abrir el archivo más de una vez al mismo tiempo, por ello existe el contador Device_Open (variable global). 
Este se incrementa cunado el archivo es abierto y se decrementa cuando es cerrado. Para poder abrir el archivo, Device_Open debe ser cero.

- encrypt_read (explicada en los comentarios):

![Code_4](Img/Picture6.png)

- encrypt_write: Es la que hace el trabajo solicitado por la consigna, se obtiene lo que ingresó el usuario/programa de usuario y se lo guarda en un buffer auxiliar (que es un array de caracteres). Luego a cada elemento de este array se le suma “valor” que es una variable global que contiene el número que se le sumará a cada carácter para lograr el encriptado de la cadena ingresada (elegimos valor = 5)

![Code_5](Img/Picture7.png)

Se utilizó get_user que obtiene una variable simple de el espacio de usuario (recordemos que se está en espacio de kernel al programar el módulo).
- Análogamente, la función decrypt_write es idéntica a esta pero en lugar de sumar un valor a cada caracter de la cadena, encriptándola, le resta este mismo valor, desencriptándola.
Dado que los dos módulos a realizar son similares y sólo varían en cuanto a nombres de funciones, major number, nombre del dispositivo/archivo y el contenido de la función write, se explicó el funcionamiento con las funciones del módulo de encriptado.

### Makefile
Para compilar los módulos fue necesario instalar los headers de Linux e implementar un Makefile ya que los módulos de kernel necesitan ser compilados de manera muy diferente a los programas de usuario regulares. Gracias a kbuild y el proceso de construcción para módulos externos cargables integrado al mecanismo de construcción estándar del kernel, resulta el simple Makefile a continuación:

![Code_6](Img/Picture8.png)

Sólo son necesarias las directivas “all:” y “clean”, pero añadimos “insert”, “test” y “remove” por cuestiones de practicidad.

### Módulo en funcionamiento
Para que los módulos funcionen se debe:
- Compilar los módulos.
- Cargar los módulos en el kernel (nótese que al hacerlo se ejecuta la función “init” con lo cual también se registra el dispositivo en cuestión).
- Crear el archivo de dispositivo de caracteres y asociarlo con sus minor y major numbers.
- Una vez completado este proceso se puede escribir y leer en el CDF para verificar si el funcionamiento es el deseado.
A continuación se muestran estos pasos para nuestro caso:


- Compilar los módulos.

![Code_7](Img/Picture9.png)

Nótese que antes de ejecutar el comando make, los únicos archivos de las carpetas son el Makefile y el código en c de los módulos.

![Code_8](Img/Picture10.png)

Han sido generados todos los archivos correspondientes. Los módulos tienen extensión .ko (utilizamos los archivos con esta extensión para cargar el módulo) en lugar de .o, la cual se distingue de los object files convencionales ya que poseen una sección .modinfo que contiene información adicional sobre dónde se guarda el módulo.
- Cargar los módulos en el kernel: Para nuestro módulo encryption.ko (idem decryption.ko) utilizamos el siguiente comando (en modo superusuario):

```bash
insmod ./encryption.ko
```

Pudimos visualizar cuáles módulos estaban cargados y comprobar que efectivamente los nuestros lo estaban con el comando:

```bash
lsmod | head
```

![Code_9](Img/Picture11.png)

- Crear el archivo de dispositivo de caracteres y asociarlo con sus minor y major numbers:

![Code_10](Img/Picture12.png)

```bash
mknod /dev/encryption_char c 143 0 
mknod /dev/decryption_char c 194 0
```

Podemos ver los CDF existentes y sus major y minor number mediante:

```bash
ls -l /dev/
```

o bien buscarlos según sus mayor number (ej: encryption -> 143) mediante:

```bash
ls -l /dev/ | grep 143
```

![Code_11](Img/Picture13.png)

- Leer y escribir los CDF para verificar que el funcionamiento sea el deseado. Se escribió “Probando el TP3 de sistemas operativos I” en el CDF para corroborar el funcionamiento del driver de encriptado. Luego se leyó el mismo archivo para obtener la cadena encriptada. Esta cadena fue escrita en el CDF para desencriptado y finalmente se leyó este archivo para obtener la cadena original:

![Code_12](Img/Picture14.png)

### Conclusión
Hemos finalizado el trabajo práctico correctamente, para hacerlo hemos utilizado los conceptos adquiridos en la materia Sistemas Operativos I respecto a módulos de kernel e utilizado la bibliografía especificada para ello. El único inconveniente con el que nos encontramos fue que cuando en una escritura de los CDF se escriben muchos caracteres y se desea reemplazar por la siguiente escritura con pocos caracteres, algunos de los caracteres permanecen en el buffer ya que este no es limpiado completamente en cada nueva escritura (no se devuelven cadenas erróneas porque al escribir, se define el final de la cadena de caracteres con un “\0”, con lo cual los módulos funcionan correctamente). No modificamos estas circunstancias ya que el funcionamiento de los módulos era el deseado y por lo tanto no lo consideramos necesario. Para visualizar este inconveniente se propone descontentar el código destinado a ello en las funciones encrypt_close() y decrypt_close().

**Nota:** Para poder trabajar se instalaron las cabeceras de Linux, el comando que utilizamos para hacerlo fue:

```bash
apt search linux-headers-$(uname -r)
```

### Referencias: 
- [Linux Kernel Module Programming Guide](http://tldp.org/LDP/lkmpg/2.6/html/index.html)
- [Linux Device Drivers, Third Edition](https://static.lwn.net/images/pdf/LDD3/ch03.pdf)
- [YouTube: Linux Kernel Programming](https://www.youtube.com/watch?v=S8hifIrDh-g&list=PL16941B715F5507C5&index=1)

